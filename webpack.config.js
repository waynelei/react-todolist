var path = require('path');
var webpack = require('webpack');


module.exports = {
  devtool: 'source-map',
  entry: [
    'webpack-dev-server/client?http://127.0.0.1:8888',
    './src'
  ],
  devServer: {
    port: 8888, // most common port
  },
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'bundle.js',
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  ],
  module: {
    loaders: [
    // js
    {
        test: /\.jsx?$/,         // Match both .js and .jsx files
        exclude: /node_modules/,
        loader: 'babel-loader',
        include: path.join(__dirname, 'src'),
        query:
          {
            presets:['react']
          }
    },
    // CSS
    {
      test: /\.styl$/,
      include: path.join(__dirname, 'src'),
      loader: 'style-loader!css-loader!stylus-loader'
    }
    ]
  }
};
