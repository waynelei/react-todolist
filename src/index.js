import React from 'react';
import {render} from 'react-dom';
import css from './styles/style.styl';
import { Provider } from 'react-redux';
import store from './store';

import App from './components/App';
import Statelayer from './components/Statelayer';

import Main from './components/Main';

import PhotoGrid from './components/PhotoGrid';
import Single from './components/Single';

var createReactClass = require('create-react-class');

import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom';

const router = (
  <Provider store = {store}>
    <Router>
      <div>
        <App />
        <Route exact path="/" component={PhotoGrid}/>
        <Route path="/view/:postId" component={Single}/>
      </div>
    </Router>
  </Provider>
);




render(
  router,
  document.getElementById('app')
);
