import React from 'react';
var createReactClass = require('create-react-class');

const HelloWorld = createReactClass({

  render() {
    return (
      <div className='single-photo'>
      	hello world!
      </div>
    )
  }

});

export default HelloWorld
