import React from 'react';
import PhotoGrid from './PhotoGrid';
import { Link } from 'react-router-dom';
import defaultState from '../store';
import posts from '../data/posts';
import comments from '../data/comments';

var createReactClass = require('create-react-class');

const Main = createReactClass({

  render(){
    return(
      <div>
        <h1>
          <Link to="/">Reduxstagegram</Link>
        </h1>
      </div>
    )
  }
});



// const Main = () => (
//     <div>
//       <h1>
//         <Link to="/">Reduxstagegram</Link>
//       </h1>
//
//         {JSON.stringify(this.props.posts, null, " ")}
//
//       <hr />
//     </div>
// )

export default Main;
