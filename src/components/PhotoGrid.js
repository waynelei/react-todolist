import React from 'react';

import { Link } from 'react-router-dom';

import posts from '../data/posts';
import comments from '../data/comments';

import Photo from './Photo';


var createReactClass = require('create-react-class');

const PhotoGrid = createReactClass({

  getInitialState: function() {
    return {
      posts: [],
      comments:[]
    };
  },


  componentDidMount() {
    this.setState({
      posts: posts,
      comments: comments
    })
  },

  renderList() {
    const { posts } = this.state;
      return (
        <div className='photo-grid'>
          {posts.map((post, index) => {

            return (
              <div key={index} >
                <Photo {...this.props} key={index} i={index} post={post} />)
              </div>
            )
          })}
        </div>
      )

  },

  render(){
    return(
      <div className='photo-grid'>
        {this.state.posts.map((post, i) => <Photo {...this.state} key={i} i={i} post={post} />)}
      </div>
    )
  }
});



//
// const PhotoGrid = () => (
//   <div>
//     <Link to="/view/fadfasfsa">view</Link>
//   </div>
//
//
// )

export default PhotoGrid;
